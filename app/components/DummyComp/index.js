/**
*
* DummyComp
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class DummyComp extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <FormattedMessage {...messages.header} />
        <br />this is an edit
      </div>
    );
  }
}

DummyComp.propTypes = {

};

export default DummyComp;

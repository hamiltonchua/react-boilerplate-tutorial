/*
 * DummyComp Messages
 *
 * This contains all the text for the DummyComp component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.DummyComp.header',
    defaultMessage: 'This is the DummyComp component !',
  },
});

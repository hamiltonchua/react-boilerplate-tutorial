/*
 *
 * PostsPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import {
  makeSelectPostsPage,
  selectPostData,
} from './selectors';
import {
  loadPostAction,
} from './actions.js';
import messages from './messages';

import DummyComp from '../../components/DummyComp';

export class PostsPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
  console.log('in index.js');
  console.log(this.props.postData);
    let myData = [];
    if (this.props.postData) {
      myData = this.props.postData
    }
    return (
      <div>
        <Helmet
          title="My Posts"
          meta={[
            { name: 'description', content: 'Description of PostsPage' },
          ]}
        />
        <FormattedMessage {...messages.header} />
        <br />this is a body
        <DummyComp />
        <a style={{ cursor: 'pointer', textDecoration: 'underline', marginLeft: '20px' }} onClick={this.props.doLoad}> this is a trigger </a>
        <ul>
        {
          myData.map((row, index) => {
            return (<li key={index}>{row.id} : {row.title}</li>);
          })
        }
        </ul>
      </div>
    );
  }
}

PostsPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  postData: PropTypes.array,
  doLoad: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  PostsPage: makeSelectPostsPage(),
  postData: selectPostData(),
});

function mapDispatchToProps(dispatch) {
  return {
    doLoad: () => {
      console.log('doLoad triggered on index.js');
      dispatch(loadPostAction());
    },
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PostsPage);

/*
 *
 * PostsPage actions
 *
 */

import {
  DEFAULT_ACTION,
  LOAD_POST,
  LOAD_POST_SUCCESS,
  LOAD_POST_FAIL,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function loadPostAction() {
  console.log('doLoad triggered on action.js');
  return {
    type: LOAD_POST,
  };
}

export function loadPostSuccessAction(data) {
  return {
    type: LOAD_POST_SUCCESS,
    data,
  };
}

export function loadPostFailAction() {
  return {
    type: LOAD_POST_FAIL,
  };
}

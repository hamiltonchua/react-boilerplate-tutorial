/*
 *
 * PostsPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  LOAD_POST,
  LOAD_POST_FAIL,
  LOAD_POST_SUCCESS,
} from './constants';

const initialState = fromJS({});

function postsPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_POST_FAIL:
      return state;
    case LOAD_POST_SUCCESS:
      console.log('load post success reducer');
      return state.set('postData', action.data);
    case LOAD_POST:
      return state;
    case DEFAULT_ACTION:
      return state;
    default:
      return state;
  }
}

export default postsPageReducer;

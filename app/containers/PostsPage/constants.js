/*
 *
 * PostsPage constants
 *
 */

export const DEFAULT_ACTION = 'app/PostsPage/DEFAULT_ACTION';

export const LOAD_POST = 'app/PostsPage/LOAD_POST';
export const LOAD_POST_SUCCESS = 'app/PostsPage/LOAD_POST_SUCCESS';
export const LOAD_POST_FAIL = 'app/PostsPage/LOAD_POST_FAIL';

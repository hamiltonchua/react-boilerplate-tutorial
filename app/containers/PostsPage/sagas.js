import { take, takeLatest, call, put, select } from 'redux-saga/effects';

import request from 'utils/request';

import {
  LOAD_POST,
  LOAD_POST_FAIL,
  LOAD_POST_SUCCESS,
} from './constants.js';

import {
  loadPostAction,
  loadPostSuccessAction,
  loadPostFailAction,
} from './actions.js';


function* loadPosts() {
  const url = 'https://jsonplaceholder.typicode.com/posts';
  const params = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const data = yield call(request, url, params);
  yield put(loadPostSuccessAction(data));
}

export function* postloadSaga() {
  yield takeLatest(LOAD_POST, loadPosts);
}

// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
}

// All sagas to be loaded
export default [
  postloadSaga,
  defaultSaga,
];
